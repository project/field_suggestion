```
  _____ _      _     _   ____                              _   _
 |  ___(_) ___| | __| | / ___| _   _  __ _  __ _  ___  ___| |_(_) ___  _ __
 | |_  | |/ _ \ |/ _` | \___ \| | | |/ _` |/ _` |/ _ \/ __| __| |/ _ \| '_ \
 |  _| | |  __/ | (_| |  ___) | |_| | (_| | (_| |  __/\__ \ |_| | (_) | | | |
 |_|   |_|\___|_|\__,_| |____/ \__,_|\__, |\__, |\___||___/\__|_|\___/|_| |_|
                                     |___/ |___/
```

# Field Suggestion

The Field Suggestion module provides a list of often-used values of a field for
selecting one from them to automatically fill the field instead of filling the
field each time manually.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/field_suggestion).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/field_suggestion).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Dynamic Entity Reference](https://www.drupal.org/project/dynamic_entity_reference)
- [Service](https://www.drupal.org/project/service)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module.

1. Set suggestions list size and select supported fields in Administration >
   Configuration > Content authoring > Field suggestion.


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
