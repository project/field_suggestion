<?php

/**
 * @file
 * Provides a list of often-used values of a field.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field_suggestion\Element\FieldSuggestionOperations;
use Drupal\field_suggestion\Element\FieldSuggestionToggle;
use Drupal\field_suggestion\FieldSuggestionBuilder;

/**
 * Implements hook_help().
 */
function field_suggestion_help(
  string $route_name,
  RouteMatchInterface $route_match,
): string {
  if ($route_name !== 'help.page.field_suggestion') {
    return '';
  }

  $translate = 't';

  return <<<HTML
<h3>{$translate('About')}</h3>
<p>{$translate('Provides a list of often-used values of a field for selecting one from them to automatically fill the field instead of filling the field each time manually.')}</p>
HTML;
}

/**
 * Implements hook_field_widget_single_element_form_alter().
 */
function field_suggestion_field_widget_single_element_form_alter(
  array &$element,
  FormStateInterface $form_state,
  array $context,
): void {
  \Drupal::classResolver(FieldSuggestionBuilder::class)
    ->alterFormFieldWidget($element, $context, $form_state);
}

/**
 * Implements hook_entity_operation().
 */
function field_suggestion_entity_operation(EntityInterface $entity): array {
  return \Drupal::classResolver(FieldSuggestionBuilder::class)
    ->entityOperation($entity);
}

/**
 * Implements hook_element_info_alter().
 */
function field_suggestion_element_info_alter(array &$info): void {
  $info['operations']['#pre_render'][] = [
    FieldSuggestionOperations::class,
    'preRender',
  ];
}

/**
 * Implements hook_theme().
 */
function field_suggestion_theme(
  array $existing,
  string $type,
  string $theme,
  string $path,
): array {
  return [
    'bootstrap_dropdown__field_suggestion' => [
      'base hook' => 'bootstrap_dropdown',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function field_suggestion_preprocess_bootstrap_dropdown__field_suggestion(array &$variables): void {
  $attributes = &$variables['toggle']['#attributes'];
  $attributes['class'][] = 'dropdown-toggle';
  $attributes['id'] = substr($attributes['data-dropdown-target'], 1);

  unset(
    $attributes['formnovalidate'],
    $attributes['data-dropdown-target'],
    $variables['items']['#items'][0],
  );

  $attributes += [
    'data-toggle' => 'dropdown',
    'aria-haspopup' => 'true',
    'aria-expanded' => 'false',
  ];

  $variables['toggle']['#value'] = Markup::create(
    "{$variables['toggle']['#value']} <span class=\"caret\"></span>",
  );

  $variables['toggle']['#split'] = FALSE;

  $variables['toggle']['#pre_render'][] = [
    FieldSuggestionToggle::class,
    'preRender',
  ];

  $variables['items']['#attributes']['aria-labelledby'] = $attributes['id'];
}

/**
 * Implements hook_preprocess_HOOK().
 */
function field_suggestion_preprocess_links__help(array &$variables): void {
  $titles = [
    'entity.field_suggestion.collection' => t('Pinned items'),
    'entity.field_suggestion_type.collection' => t('Supported field types'),
    'field_suggestion.settings' => t('Settings'),
  ];

  foreach ($variables['links'] as &$item) {
    /** @var \Drupal\Core\Url $url */
    $url = $item['link']['#url'];

    if (isset($titles[$route_name = $url->getRouteName()])) {
      $item['link']['#title'] = $titles[$route_name];
    }
  }
}
