<?php

namespace Drupal\field_suggestion;

/**
 * Enum for supported action types.
 */
enum Action: string {

  case Pin = 'pin';
  case Ignore = 'ignore';

  /**
   * Gets title for the current state.
   *
   * @param bool $active
   *   TRUE, if the operation was applied.
   */
  public function label(bool $active): string {
    return match ($this) {
      Action::Pin => $active ? t('Unpin') : t('Pin'),
      Action::Ignore => $active ? t('Include') : t('Ignore'),
    };
  }

  /**
   * Gets permission for using operation.
   */
  public function permission(): string {
    return match ($this) {
      Action::Pin => 'pin and unpin field suggestion',
      Action::Ignore => 'ignore field suggestion',
    };
  }

}
