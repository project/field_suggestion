<?php

namespace Drupal\field_suggestion\Service;

/**
 * Defines the field values filter service.
 */
class FieldSuggestionValueFilter extends FieldSuggestionFilterBase implements FieldSuggestionValueFilterInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(string $entity_type, string $field_name): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function items(string $entity_type, string $field_name): array {
    $fields = [];

    foreach ($this->filters as $filter_id) {
      $filter = $this->classResolver->getInstanceFromDefinition($filter_id);

      if ($filter->applies($entity_type, $field_name)) {
        array_push($fields, ...$filter->items($entity_type, $field_name));
      }
    }

    return $fields;
  }

}
