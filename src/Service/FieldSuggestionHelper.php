<?php

namespace Drupal\field_suggestion\Service;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Defines the helper service.
 */
class FieldSuggestionHelper implements FieldSuggestionHelperInterface {

  /**
   * FieldSuggestionHelper constructor.
   *
   * @param \Drupal\field_suggestion\Service\FieldSuggestionValueFilterInterface $filter
   *   The field values filter.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   */
  public function __construct(
    protected FieldSuggestionValueFilterInterface $filter,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function ignored(string $entity_type, string $field_name): array {
    $storage = $this->entityTypeManager->getStorage('field_suggestion');

    $ids = $storage->getQuery()
      ->accessCheck()
      ->condition('ignore', TRUE)
      ->condition('entity_type', $entity_type)
      ->condition('field_name', $field_name)
      ->execute();

    return [
      ...$this->filter->items($entity_type, $field_name),
      ...array_filter(array_map(
        fn(string $id): ?string => $storage->load($id)?->label(),
        $ids,
      )),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function bundle(string $field_type): void {
    $this->entityTypeManager->getStorage('field_suggestion_type')
      ->create(['id' => $field_type])
      ->save();

    /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
    $field_storage = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'type' => $field_type,
        'entity_type' => 'field_suggestion',
        'field_name' => $this->field($field_type),
      ]);

    $field_storage->save();

    $this->entityTypeManager->getStorage('field_config')->create([
      'field_storage' => $field_storage,
      'bundle' => $field_type,
      'label' => 'Suggestion',
      'required' => TRUE,
    ])->save();

    $this->entityDisplayRepository
      ->getFormDisplay('field_suggestion', $field_type)
      ->setComponent($field_storage->getName(), ['weight' => 0])
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function field(string $type): string {
    $name = "field_suggestion_$type";

    if (mb_strlen($name) > FieldStorageConfig::NAME_MAX_LENGTH) {
      $name = mb_substr($name, 0, FieldStorageConfig::NAME_MAX_LENGTH);
    }

    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public static function encode($data): string {
    return str_replace('_', '-', $data);
  }

  /**
   * {@inheritdoc}
   */
  public static function decode($raw): string {
    return str_replace('-', '_', $raw);
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension(): string {
    return '';
  }

}
