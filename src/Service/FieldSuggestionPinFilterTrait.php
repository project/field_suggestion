<?php

namespace Drupal\field_suggestion\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the Field Suggestion pin filter service.
 */
trait FieldSuggestionPinFilterTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFieldSuggestionPinFilter = 'field_suggestion.filter.pin';

  /**
   * Sets the Field Suggestion pin filter.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFieldSuggestionPinFilter(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the Field Suggestion pin filter.
   */
  protected function fieldSuggestionPinFilter(): FieldSuggestionPinFilterInterface {
    return $this->getKnownService();
  }

}
