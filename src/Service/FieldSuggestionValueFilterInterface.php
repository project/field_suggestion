<?php

namespace Drupal\field_suggestion\Service;

/**
 * Defines the field values filter service interface.
 */
interface FieldSuggestionValueFilterInterface {

  /**
   * Whether this filter should be used to exclude field values.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   TRUE if this ignorer should be used or FALSE to let other filters decide.
   */
  public function applies(string $entity_type, string $field_name): bool;

  /**
   * Provides field values that should be excluded from the suggestions list.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   */
  public function items(string $entity_type, string $field_name): array;

}
