<?php

namespace Drupal\field_suggestion\Service;

use Drupal\service\ServiceTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the Field Suggestion helper service.
 */
trait FieldSuggestionHelperTrait {

  use ServiceTrait;

  /**
   * The service name.
   */
  protected const string serviceFieldSuggestionHelper = 'field_suggestion.helper';

  /**
   * Sets the Field Suggestion helper.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   (optional) The service container. Defaults to NULL.
   */
  protected function addFieldSuggestionHelper(?ContainerInterface $container = NULL): static {
    return $this->setKnownService($container);
  }

  /**
   * Gets the Field Suggestion helper.
   */
  protected function fieldSuggestionHelper(): FieldSuggestionHelperInterface {
    return $this->getKnownService();
  }

}
