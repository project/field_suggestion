<?php

namespace Drupal\field_suggestion\Service;

use Drupal\Core\DependencyInjection\ClassResolverInterface;

/**
 * Defines the filter service.
 */
abstract class FieldSuggestionFilterBase {

  /**
   * Constructs a new FieldSuggestionFilterBase.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   * @param string[] $filters
   *   (optional) Holds an array of filter IDs, sorted by priority. Defaults to
   *   an empty array.
   */
  public function __construct(
    protected ClassResolverInterface $classResolver,
    protected array $filters = [],
  ) {}

}
