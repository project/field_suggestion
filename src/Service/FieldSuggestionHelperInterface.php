<?php

namespace Drupal\field_suggestion\Service;

use Drupal\Component\Serialization\SerializationInterface;

/**
 * Defines the helper service interface.
 */
interface FieldSuggestionHelperInterface extends SerializationInterface {

  /**
   * The cache tag.
   */
  public const string TAG = 'field_suggestion_operations';

  /**
   * Gets field values that should be excluded from the suggestions list.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   *
   * @return string[]
   *   The values list.
   */
  public function ignored(string $entity_type, string $field_name): array;

  /**
   * Creates a bundle.
   *
   * @param string $field_type
   *   The field type.
   */
  public function bundle(string $field_type): void;

  /**
   * Creates the name of the field for storing suggestion value.
   *
   * @param string $type
   *   The type name.
   */
  public function field(string $type): string;

}
