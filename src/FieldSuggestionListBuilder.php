<?php

namespace Drupal\field_suggestion;

use Drupal\Core\Entity\EntityInterface;
use Drupal\service\EntityFieldManagerTrait;
use Drupal\service\EntityListBuilderBase;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Defines a class to build a listing of field suggestion entities.
 *
 * @see \Drupal\field_suggestion\Entity\FieldSuggestion
 */
class FieldSuggestionListBuilder extends EntityListBuilderBase {

  use EntityFieldManagerTrait;
  use EntityTypeManagerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addEntityFieldManager()
      ->addEntityTypeManager()
      ->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'type' => $this->t('Type'),
      'entity_type' => $this->t('Entity type'),
      'field_name' => $this->t('Field name'),
      'field_value' => $this->t('Field value'),
      'usage' => $this->t('Usage'),
      'exclude' => $this->t('Exclude'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    if (!$entity instanceof FieldSuggestionInterface) {
      return parent::buildRow($entity);
    }

    $ignore = $entity->isIgnored();
    $row = ['type' => $ignore ? $this->t('Ignored') : $this->t('Pinned')];

    $row['entity_type'] = $this->label(
      $this->entityTypeManager()->getDefinitions(),
      $entity_type = $entity->type(),
    );

    $row['field_name'] = $this->label(
      $this->entityFieldManager()->getBaseFieldDefinitions($entity_type),
      $entity->field(),
    );

    $row['field_value'] = $entity->label();
    $row['usage'] = $ignore ? '-' : ($entity->isOnce() ? 1 : '∞');
    $row['exclude'] = $ignore ? '-' : $entity->countExcluded();

    return $row + parent::buildRow($entity);
  }

  /**
   * Provides cell text based on a labeled object as a field.
   *
   * @param array $definitions
   *   The definitions of entity types or fields.
   * @param string $name
   *   The name of entity type or field.
   *
   * @return string
   *   The label if a definition is found. Otherwise, a received name.
   */
  protected function label(array $definitions, string $name): string {
    if (isset($definitions[$name])) {
      $name = "{$definitions[$name]->getLabel()}  ($name)";
    }

    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t('There are no field suggestions yet.');

    return $build;
  }

}
