<?php

namespace Drupal\field_suggestion\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_suggestion\Service\FieldSuggestionHelperTrait;
use Drupal\service\ConfigFormBase;
use Drupal\service\EntityFieldManagerTrait;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Displays the Field Suggestion settings form.
 */
class FieldSuggestionSettingsForm extends ConfigFormBase {

  use EntityFieldManagerTrait;
  use EntityTypeManagerTrait;
  use FieldSuggestionHelperTrait;
  use StringTranslationTrait;

  /**
   * The allowed fields.
   */
  protected array $allowedFields;

  /**
   * The allowed types.
   */
  protected array $allowedTypes;

  /**
   * The entity type identifier.
   */
  protected string $entityTypeId;

  /**
   * The field types.
   */
  protected array $fieldTypes;

  /**
   * The field names duplication resolver.
   *
   * Shows which entity type fields have an extra character in the name to not
   * duplicate the name of other fields.
   *
   * @var bool[]
   */
  protected array $hasSuffix = [];

  /**
   * The selected fields.
   */
  protected array $selectedFields;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addEntityFieldManager()
      ->addEntityTypeManager()
      ->addFieldSuggestionHelper()
      ->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['field_suggestion.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'field_suggestion_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $config = $this->config($this->getEditableConfigNames()[0]);

    $form['collapsible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collapsible'),
      '#description' => $this->t('Display suggestions list in drop-down view.'),
      '#default_value' => $config->get('collapsible'),
    ];

    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('The number of displayed values in a list of suggestions.'),
      '#default_value' => $config->get('limit'),
      '#min' => 1,
    ];

    $form['own'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter by owner'),
      '#description' => $this->t('Looking for suggestions only in entities of the current user.'),
      '#default_value' => $config->get('own'),
    ];

    $form['entity_types'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Entity types'),
      '#attached' => [
        'library' => ['field_suggestion/base'],
      ],
    ];

    $this->selectedFields = (array) $config->get('fields');

    if (!empty($this->selectedFields)) {
      $form['entity_types']['#default_tab'] = 'edit-' .
        str_replace('_', '-', key($this->selectedFields));
    }

    $storage = $this->entityTypeManager()->getStorage('field_suggestion_type');
    $this->fieldTypes = $storage->getQuery()->accessCheck()->execute();

    $definitions = $this->entityTypeManager()->getDefinitions();

    unset($definitions['field_suggestion']);

    foreach ($definitions as $this->entityTypeId => $entity_type) {
      if (!$entity_type instanceof ContentEntityTypeInterface) {
        continue;
      }

      $this->allowedFields = $this->allowedTypes = [];

      $this->process(
        'fields',
        $this->entityFieldManager()
          ->getBaseFieldDefinitions($this->entityTypeId),
      );

      if (!empty($this->allowedFields)) {
        $this->allowedFields['fields']['title'] = $this->t('Base fields');
      }

      if (($bundle = $entity_type->getBundleEntityType()) !== NULL) {
        $bundles_storage = $this->entityTypeManager()->getStorage($bundle);
        $bundles = $bundles_storage->getQuery()->accessCheck(FALSE)->execute();

        foreach ($bundles as $bundle) {
          $this->process(
            $bundle,
            array_filter(
              $this->entityFieldManager()
                ->getFieldDefinitions($this->entityTypeId, $bundle),
              fn(FieldDefinitionInterface $field): bool =>
                !$field instanceof BaseFieldDefinition,
            ),
          );

          if (isset($this->allowedFields[$bundle])) {
            $entity = $bundles_storage->load($bundle);
            $replacements = [];

            if ($entity !== NULL) {
              $phrase = '%label (%bundle) bundle-specific fields';
              $replacements['%label'] = $entity->label();
              $replacements['%bundle'] = $bundle;
            }
            else {
              $phrase = '%label bundle-specific fields';
              $replacements['%label'] = $bundle;
            }

            $this->allowedFields[$bundle]['title'] = $this->t(
              $phrase,
              $replacements,
            );
          }
        }
      }

      if (empty($this->allowedFields)) {
        continue;
      }

      $this->hasSuffix[$this->entityTypeId] = isset($form[$this->entityTypeId]);

      $key = $this->entityTypeId;

      if ($this->hasSuffix[$this->entityTypeId]) {
        $key .= '_';
      }

      $form[$key] = [
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#group' => 'entity_types',
        '#tree' => TRUE,
      ];

      foreach ($this->allowedFields as $bundle => $fields) {
        $form[$key][$bundle] = [
          '#type' => 'checkboxes',
          '#title' => $fields['title'],
          '#options' => $fields['items'],
          '#default_value' => $this->allowedTypes[$bundle] ?? [],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $all_fields = [];

    $field_types = $this->entityTypeManager()
      ->getStorage('field_suggestion_type')
      ->getQuery()
      ->accessCheck()
      ->execute();

    foreach ($this->hasSuffix as $entity_type => $has_suffix) {
      $entity_key = $entity_type . ($has_suffix ? '_' : '');

      foreach ($form_state->getValue($entity_key) as $bundle => $fields) {
        foreach (array_filter(array_values($fields)) as $field) {
          [$field_name, $field_type] = explode('|', $field);

          if ($bundle === 'fields') {
            $all_fields[$entity_type]['base'][] = $field_name;
          }
          else {
            $all_fields[$entity_type]['bundles'][$bundle][] = $field_name;
          }

          if (!in_array($field_type, $field_types)) {
            $this->fieldSuggestionHelper()->bundle($field_type);
            $field_types[] = $field_type;
          }
        }
      }
    }

    $config = $this->config($this->getEditableConfigNames()[0])
      ->set('fields', $all_fields);

    foreach (['collapsible', 'limit', 'own'] as $field) {
      $config->set($field, $form_state->getValue($field));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Extends a list of supported fields.
   *
   * @param string $bundle
   *   The bundle entity type identifier.
   * @param array $definitions
   *   The field definitions.
   */
  protected function process(string $bundle, array $definitions): void {
    $parents = [$this->entityTypeId, $bundle === 'fields' ? 'base' : 'bundles'];

    if ($bundle !== 'fields') {
      $parents[] = $bundle;
    }

    $fields = (array) NestedArray::getValue($this->selectedFields, $parents);

    foreach ($definitions as $field_name => $field) {
      if (!$field->isReadOnly() && $field->getDisplayOptions('form') !== NULL) {
        $key = "$field_name|" . ($field_type = $field->getType());
        $this->allowedFields[$bundle]['items'][$key] = $field->getLabel();

        if (
          in_array($field_name, $fields) &&
          in_array($field_type, $this->fieldTypes)
        ) {
          $this->allowedTypes[$bundle][] = $key;
        }
      }
    }
  }

}
