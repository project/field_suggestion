<?php

namespace Drupal\field_suggestion;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface defining a field suggestion entity.
 */
interface FieldSuggestionInterface extends ContentEntityInterface, EntityPublishedInterface {

  /**
   * Gets a related entity type identifier.
   */
  public function type(): string;

  /**
   * Gets a related field name.
   */
  public function field(): string;

  /**
   * Checks if it's an ignored entity.
   */
  public function isIgnored(): bool;

  /**
   * Marks a suggestion as ignored or pinned.
   *
   * @param bool $ignored
   *   The suggestion type will be the following:
   *   - TRUE: The ignored suggestion.
   *   - FALSE: The pinned suggestion.
   */
  public function setIgnored(bool $ignored): static;

  /**
   * Checks if it has excluded entities.
   */
  public function hasExcluded(): bool;

  /**
   * Returns set of excluded entities.
   */
  public function getExcluded(): array;

  /**
   * Gets the amount of excluded entities.
   */
  public function countExcluded(): int;

  /**
   * Checks if a suggestion can be used only one time.
   *
   * @return bool
   *   TRUE, if so.
   */
  public function isOnce();

  /**
   * Marks a suggestion as one that can only be used one time.
   *
   * @return $this
   */
  public function setOnce();

  /**
   * Gets value.
   *
   * @param bool $original
   *   (optional) TRUE if the identifier of referenced entity should not be
   *   replaced by a label. Defaults to FALSE.
   */
  public function label(bool $original = FALSE): string;

}
