<?php

namespace Drupal\field_suggestion;

use Drupal\Core\Entity\EntityInterface;
use Drupal\service\ConfigEntityListBuilderBase;
use Drupal\service\FieldTypePluginManagerTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Defines a class to build a listing of field suggestion type entities.
 *
 * @see \Drupal\field_suggestion\Entity\FieldSuggestionType
 */
class FieldSuggestionTypeListBuilder extends ConfigEntityListBuilderBase {

  use FieldTypePluginManagerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addFieldTypePluginManager()->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'id' => $this->t('Machine name'),
      'label' => $this->t('Title'),
      'description' => $this->t('Description'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $definition = $this->fieldTypePluginManager()->getDefinition($entity->id());

    return [
      'id' => $entity->id(),
      'label' => $entity->label(),
      'description' => $definition['description'],
    ] + parent::buildRow($entity);
  }

}
