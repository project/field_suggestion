<?php

namespace Drupal\field_suggestion\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\field_suggestion\Action;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting operation names to enumeration object.
 */
class FieldSuggestionConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): Action {
    return Action::from($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return !empty($definition['type']) &&
      $definition['type'] === 'field_suggestion_action';
  }

}
