<?php

namespace Drupal\field_suggestion\Element;

use Drupal\field_suggestion\Service\FieldSuggestionHelperInterface;

/**
 * Provides an operations form element.
 */
class FieldSuggestionOperations extends FieldSuggestionElementBase {

  /**
   * {@inheritdoc}
   */
  public static function preRender(array $element): array {
    $element['#cache']['tags'][] = FieldSuggestionHelperInterface::TAG;
    return $element;
  }

}
