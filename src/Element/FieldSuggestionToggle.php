<?php

namespace Drupal\field_suggestion\Element;

/**
 * Provides a toggle form element.
 */
class FieldSuggestionToggle extends FieldSuggestionElementBase {

  /**
   * {@inheritdoc}
   */
  public static function preRender(array $element): array {
    $element['#attributes']['type'] = 'button';
    return $element;
  }

}
