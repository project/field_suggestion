<?php

namespace Drupal\field_suggestion\Element;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides a base class for a form element.
 */
abstract class FieldSuggestionElementBase implements TrustedCallbackInterface {

  /**
   * #pre_render callback.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   element.
   */
  abstract public static function preRender(array $element): array;

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRender'];
  }

}
