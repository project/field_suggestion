<?php

namespace Drupal\field_suggestion;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait as CoreStringTranslationTrait;
use Drupal\Core\Url;
use Drupal\field_suggestion\Service\FieldSuggestionHelperTrait;
use Drupal\field_suggestion\Service\FieldSuggestionPinFilterTrait;
use Drupal\service\ClassResolverBase;
use Drupal\service\ConfigFactoryTrait;
use Drupal\service\CurrentUserTrait;
use Drupal\service\EntityFieldManagerTrait;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\RedirectDestinationTrait;
use Drupal\service\RendererTrait;
use Drupal\service\StringTranslationTrait;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides hook wrappers.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class FieldSuggestionBuilder extends ClassResolverBase {

  use ConfigFactoryTrait;
  use CoreStringTranslationTrait;
  use CurrentUserTrait;
  use EntityFieldManagerTrait;
  use EntityTypeManagerTrait;
  use FieldSuggestionHelperTrait;
  use FieldSuggestionPinFilterTrait;
  use RedirectDestinationTrait;
  use RendererTrait;

  use StringTranslationTrait {
    StringTranslationTrait::getStringTranslation insteadof CoreStringTranslationTrait;
  }

  /**
   * The field child element identifier for suggestions.
   */
  protected const string SELECTOR = 'suggestion';

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addConfigFactory()
      ->addCurrentUser()
      ->addEntityFieldManager()
      ->addEntityTypeManager()
      ->addFieldSuggestionHelper()
      ->addFieldSuggestionPinFilter()
      ->addRedirectDestination()
      ->addRenderer()
      ->addStringTranslation();
  }

  /**
   * Alter forms for field widgets provided by other modules.
   *
   * @param array $element
   *   The field widget form element.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *   - form: The form structure to which widgets are being attached.
   *   - widget: The widget plugin instance.
   *   - items: The field values.
   *   - delta: The order of this item in the array of sub-elements.
   *   - default: A boolean indicating whether the form is being shown as a
   *     dummy form to set default values.
   *
   * @see field_suggestion_field_widget_single_element_form_alter()
   */
  public function alterFormFieldWidget(array &$element, array $context): void {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $items = $context['items'];

    $config = $this->configFactory()->get('field_suggestion.settings');
    $fields = (array) $config->get('fields');

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $items->getEntity();

    if (!isset($fields[$entity_type_id = $entity->getEntityTypeId()])) {
      return;
    }

    $definition = $items->getFieldDefinition();
    $bundle = $definition->getTargetBundle();

    $parents = [
      $entity_type_id,
      $bundle !== NULL ? 'bundles' : 'base',
      $bundle,
    ];

    $fields = (array) NestedArray::getValue($fields, array_filter($parents));

    if (!in_array($field_name = $items->getName(), $fields)) {
      return;
    }

    $related_storage = $this->entityTypeManager()->getStorage($entity_type_id);

    $query = $related_storage
      ->getAggregateQuery()
      ->accessCheck()
      ->addTag('field_suggestion')
      ->addMetaData('field_name', $field_name)
      ->addMetaData('entity', $entity);

    if ($config->get('own')) {
      $entity_type = $entity->getEntityType();

      if (
        is_subclass_of($entity_type->getClass(), EntityOwnerInterface::class) &&
        $entity_type->hasKey('owner')
      ) {
        $query->condition(
          $entity_type->getKey('owner'),
          $this->currentUser()->id(),
        );
      }
    }

    $ignored = $this->fieldSuggestionHelper()
      ->ignored($entity_type_id, $field_name);

    $pinned = [];
    $storage = $this->entityTypeManager()->getStorage('field_suggestion');

    $ids = ($ignored_query = $storage->getQuery())
      ->accessCheck()
      ->condition(
        $ignored_query->orConditionGroup()
          ->notExists('ignore')
          ->condition('ignore', FALSE),
      )
      ->condition('entity_type', $entity_type_id)
      ->condition('field_name', $field_name)
      ->execute();

    foreach ((array) $ids as $id) {
      /** @var \Drupal\field_suggestion\FieldSuggestionInterface $suggestion_entity */
      $suggestion_entity = $storage->load($id);

      $allow = TRUE;

      if (
        $suggestion_entity->hasExcluded() &&
        $this->fieldSuggestionPinFilter()
          ->exclude($suggestion_entity->getExcluded(), $entity)
      ) {
        $allow = FALSE;
      }

      $value = $suggestion_entity->label(TRUE);

      if ($allow && $suggestion_entity->isOnce()) {
        $count = (int) $related_storage
          ->getQuery()
          ->addTag('field_suggestion')
          ->addTag('field_suggestion_usage')
          ->addMetaData('field_name', $field_name)
          ->addMetaData('entity', $entity)
          ->condition($field_name, $value)
          ->range(0, 1)
          ->count()
          ->accessCheck()
          ->execute();

        if ($count === 1) {
          $allow = FALSE;
        }
      }

      if ($allow) {
        $pinned[] = $value;
      }
      else {
        $ignored[] = $value;
      }
    }

    $ignored = array_unique([...$ignored, ...$pinned]);

    if (!empty($ignored)) {
      $query->condition($field_name, $ignored, 'NOT IN');
    }

    $values = $query
      ->sortAggregate($field_name, 'COUNT', 'DESC')
      ->groupBy($field_name)
      ->range(0, $config->get('limit'))
      ->execute();

    $values = array_filter(array_column($values, $field_name));

    if (!empty($pinned)) {
      array_unshift($values, ...$pinned);
    }

    if (count($values) === 0) {
      return;
    }

    if ($config->get('collapsible')) {
      $suggestion_element = [
        '#type' => 'dropbutton',
        '#subtype' => 'field_suggestion',
        '#attached' => ['library' => ['field_suggestion/ajax']],
        '#links' => [['title' => $this->t('Suggestions')]],
      ];

      $url = Url::fromRoute('field_suggestion.select')
        ->setRouteParameters([
          'entity_type' => $this->fieldSuggestionHelper()
            ->encode($entity_type_id),
          'field_name' => $field_name,
        ])
        ->setOption('attributes', ['class' => ['use-ajax']]);
    }
    else {
      $suggestion_element = [
        '#type' => 'radios',
        '#title' => $this->t('Suggestions'),
        '#ajax' => [
          'wrapper' => "edit-$field_name-wrapper",
          'callback' => [static::class, 'ajaxCallback'],
        ],
      ];
    }

    $view_builder = $this->entityTypeManager()->getViewBuilder($entity_type_id);
    $display_options = [];

    if (is_subclass_of(
      $definition->getClass(),
      EntityReferenceFieldItemListInterface::class,
    )) {
      $item_storage = $this->entityTypeManager()->getStorage(
        $definition->getSetting('target_type'),
      );

      $display_options['settings']['link'] = FALSE;
    }

    foreach ($values as $delta => $value) {
      $ids = $related_storage
        ->getQuery()
        ->accessCheck()
        ->condition($field_name, $value)
        ->range(0, 1)
        ->execute();

      if (empty($ids)) {
        continue;
      }

      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $related_storage->load(reset($ids));

      $suggestion_item = $view_builder->viewFieldItem(
        $entity->$field_name->first(),
        $display_options,
      );

      if (isset($url)) {
        $suggestion_element['#links'][] = [
          'title' => $suggestion_item,
          'url' => (clone $url)->setRouteParameter('delta', $delta),
        ];
      }
      else {
        $suggestion_element['#options'][] = $this->renderer()
          ->render($suggestion_item);
      }

      if (isset($item_storage)) {
        $value = "{$item_storage->load($value)?->label()} ($value)";
      }

      $suggestion_element['#attributes']["data-suggestion-$delta"] = $value;
    }

    $element[static::SELECTOR] = $suggestion_element;
  }

  /**
   * Declares entity operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which the linked operations will be performed.
   *
   * @see field_suggestion_entity_operation()
   */
  public function entityOperation(EntityInterface $entity): array {
    $operations = [];

    $fields = (array) $this->configFactory()->get('field_suggestion.settings')
      ->get('fields');

    if (empty($fields[$entity_type = $entity->getEntityTypeId()]['base'])) {
      return $operations;
    }

    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $definitions */
    $definitions = $this->entityFieldManager()
      ->getBaseFieldDefinitions($entity_type);

    $fields = array_flip($fields[$entity_type]['base']);

    foreach ($definitions as $field => $definition) {
      if (isset($fields[$field])) {
        $fields[$field] = $definition->getType();
      }
    }

    $options = ['query' => $this->getRedirectDestination()->getAsArray()];
    $operations_count = 0;
    $weight = 150;
    $has_suggestions = [];

    $route_parameters = [
      'entity_type' => $this->fieldSuggestionHelper()->encode($entity_type),
      'entity_id' => $entity->id(),
    ];

    foreach ($fields as $field_name => $field_type) {
      $route_parameters['field_name'] = $field_name;
      $operations_updated = FALSE;
      $property = $definitions[$field_name]->getMainPropertyName() ?? 'value';

      foreach (Action::cases() as $action) {
        $url = Url::fromRoute(
          'field_suggestion.operation',
          ['type' => $action->value] + $route_parameters,
          $options,
        );

        if (!$url->access()) {
          continue;
        }

        $last_field_name = $field_name;

        if (!$operations_updated) {
          $operations_updated = TRUE;
          $operations_count++;
        }

        $suggestions_count = $this->entityTypeManager()
          ->getStorage('field_suggestion')
          ->getQuery()
          ->condition('ignore', $action === Action::Ignore)
          ->condition('entity_type', $entity_type)
          ->condition('field_name', $field_name)
          ->condition(
            $this->fieldSuggestionHelper()->field($field_type),
            $entity->$field_name->$property,
          )
          ->range(0, 1)
          ->count()
          ->accessCheck()
          ->execute();

        $has_suggestions[$action->value] = $suggestions_count > 0;

        $operations["field_suggestion_{$action->value}_$field_name"] = [
          'title' => sprintf(
            '%s %s',
            $action->label($has_suggestions[$action->value]),
            $field_name,
          ),
          'url' => $url,
          'weight' => $weight += 10,
        ];
      }
    }

    if (count($fields) === 1 && $operations_count === 1) {
      foreach ($has_suggestions as $action_type => $has_per_action) {
        $key = "field_suggestion_{$action_type}_$last_field_name";

        $operations[$key]['title'] = Action::from($action_type)
          ->label($has_per_action);
      }
    }

    return $operations;
  }

  /**
   * AJAX callback wrapper.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function ajaxCallback(
    array $form,
    FormStateInterface $form_state,
  ): AjaxResponse {
    return \Drupal::classResolver(static::class)
      ->doAjaxCallback($form, $form_state);
  }

  /**
   * AJAX callback.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function doAjaxCallback(
    array $form,
    FormStateInterface $form_state,
  ): AjaxResponse {
    $element = $form_state->getTriggeringElement();

    $parents = $element['#parents'];
    $name = $parents[$id = count($parents) - 3];
    $delta = $form_state->getValue($parents);

    $parents = $element['#array_parents'];
    $parents[count($parents) - 1] = '#attributes';
    $attributes = NestedArray::getValue($form, $parents);

    $selector = substr(
      $attributes['data-drupal-selector'],
      0,
      -strlen(static::SELECTOR),
    );

    if (!$id) {
      /** @var \Drupal\Core\Entity\ContentEntityFormInterface $form_object */
      $form_object = $form_state->getFormObject();

      /** @var \Drupal\Core\Field\BaseFieldDefinition $definition */
      $definition = $this->entityFieldManager()->getBaseFieldDefinitions(
        $form_object->getEntity()->getEntityTypeId(),
      )[$name];

      $property = $definition->getMainPropertyName();
    }

    $selector .= !empty($property) ? str_replace('_', '-', $property) : 'value';

    return (new AjaxResponse())->addCommand(new InvokeCommand(
      "[data-drupal-selector=\"$selector\"]",
      'val',
      [$attributes["data-suggestion-$delta"]],
    ));
  }

}
