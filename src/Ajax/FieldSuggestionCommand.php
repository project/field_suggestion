<?php

namespace Drupal\field_suggestion\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an AJAX command to select a suggestion.
 *
 * @ingroup ajax
 */
class FieldSuggestionCommand implements CommandInterface {

  /**
   * Constructs an FieldSuggestionCommand object.
   *
   * @param string $name
   *   The field name.
   * @param int $delta
   *   The suggestion offset.
   * @param string $property
   *   The name of the main property.
   */
  public function __construct(
    protected string $name,
    protected int $delta,
    protected string $property,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    return [
      'command' => 'fieldSuggestion',
      'name' => $this->name,
      'delta' => $this->delta,
      'property' => $this->property,
    ];
  }

}
