<?php

namespace Drupal\field_suggestion\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_suggestion\Action;
use Drupal\field_suggestion\Ajax\FieldSuggestionCommand;
use Drupal\field_suggestion\FieldSuggestionInterface;
use Drupal\field_suggestion\Service\FieldSuggestionHelperInterface;
use Drupal\field_suggestion\Service\FieldSuggestionHelperTrait;
use Drupal\service\CacheTagsInvalidatorTrait;
use Drupal\service\ControllerBase;
use Drupal\service\EntityFieldManagerTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for field_suggestion pages.
 */
class FieldSuggestionController extends ControllerBase {

  use CacheTagsInvalidatorTrait;
  use EntityFieldManagerTrait;
  use FieldSuggestionHelperTrait;

  /**
   * The field definition.
   */
  protected FieldDefinitionInterface $definition;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addCacheTagsInvalidator()
      ->addEntityFieldManager()
      ->addFieldSuggestionHelper();
  }

  /**
   * Provides an edit title callback.
   *
   * @param \Drupal\field_suggestion\FieldSuggestionInterface $field_suggestion
   *   The field suggestion entity object.
   */
  public function title(FieldSuggestionInterface $field_suggestion): TranslatableMarkup {
    $definitions = $this->entityFieldManager()->getBaseFieldDefinitions(
      $entity_type = $field_suggestion->type(),
    );

    return $this->t(
      'Edit a suggestion based on the %value value of the %field field of the %type entity type',
      [
        '%value' => $field_suggestion->label(),
        '%field' => $definitions[$field_suggestion->field()]->getLabel(),
        '%type' => $this->entityTypeManager()->getDefinition($entity_type)
          ?->getLabel(),
      ],
    );
  }

  /**
   * Choose a specific suggestion.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   * @param int $delta
   *   The suggestion offset.
   */
  public function select(
    string $entity_type,
    string $field_name,
    int $delta,
  ): AjaxResponse {
    return (new AjaxResponse())->addCommand(new FieldSuggestionCommand(
      $field_name,
      $delta,
      $this->fieldSuggestionHelper()
        ->encode($this->property($entity_type, $field_name)),
    ));
  }

  /**
   * Access check based on whether a field is supported or not.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   * @param int $delta
   *   The suggestion offset.
   */
  public function selectAccess(
    string $entity_type,
    string $field_name,
    int $delta,
  ): AccessResultInterface {
    $config = $this->config('field_suggestion.settings');
    $field_names = (array) $config->get('fields');
    $entity_type = $this->fieldSuggestionHelper()->decode($entity_type);
    $field_names = $field_names[$entity_type]['base'] ?? [];

    return AccessResult::allowedIf(in_array($field_name, $field_names));
  }

  /**
   * Pin or ignore values of selected fields.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param int $entity_id
   *   The entity identifier.
   * @param string $field_name
   *   The field name.
   * @param \Drupal\field_suggestion\Action $type
   *   The action type.
   */
  public function operation(
    string $entity_type,
    int $entity_id,
    string $field_name,
    Action $type,
  ): RedirectResponse {
    $property = $this->property($entity_type, $field_name);

    $value = $this->entityTypeManager()->getStorage($entity_type)
      ->load($entity_id)
      ?->$field_name
      ->$property;

    $storage = $this->entityTypeManager()->getStorage('field_suggestion');

    $entities = $storage->loadByProperties($values = [
      'type' => $field_type = $this->definition->getType(),
      'ignore' => $type === Action::Ignore,
      'entity_type' => $entity_type,
      'field_name' => $field_name,
      $this->fieldSuggestionHelper()->field($field_type) => $value,
    ]);

    if (!empty($entities)) {
      $storage->delete($entities);
    }
    else {
      $values['ignore'] = !$values['ignore'];
      $entities = $storage->loadByProperties($values);
      $values['ignore'] = !$values['ignore'];

      if (!empty($entities)) {
        /** @var \Drupal\field_suggestion\FieldSuggestionInterface $entity */
        foreach ($entities as $entity) {
          $entity->setIgnored($values['ignore'])->save();
        }
      }
      else {
        $storage->create($values)->save();
      }
    }

    $this->cacheTagsInvalidator()
      ->invalidateTags([FieldSuggestionHelperInterface::TAG]);

    return $this->redirect('<front>');
  }

  /**
   * Access check based on whether a field is supported or not.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param int $entity_id
   *   The entity identifier.
   * @param string $field_name
   *   The field name.
   * @param \Drupal\field_suggestion\Action $type
   *   The action type.
   */
  public function operationAccess(
    string $entity_type,
    int $entity_id,
    string $field_name,
    Action $type,
  ): AccessResultInterface {
    if (
      !$this->currentUser()->hasPermission('administer field suggestion') &&
      !$this->currentUser()->hasPermission($type->permission()) ||
      !$this->selectAccess($entity_type, $field_name, 0)->isAllowed()
    ) {
      return AccessResult::neutral();
    }

    $entity_type = $this->fieldSuggestionHelper()->decode($entity_type);

    $entity = $this->entityTypeManager()->getStorage($entity_type)
      ->load($entity_id);

    if ($entity === NULL || ($field = $entity->$field_name)->isEmpty()) {
      return AccessResult::neutral();
    }

    $property = $this->property($entity_type, $field_name);

    $count = $this->entityTypeManager()->getStorage('field_suggestion')
      ->getQuery()
      ->accessCheck()
      ->condition('entity_type', $entity_type)
      ->condition('field_name', $field_name)
      ->condition(
        $this->fieldSuggestionHelper()->field($this->definition->getType()),
        $field->$property,
      )
      ->range(0, 1)
      ->count()
      ->execute();

    return AccessResult::allowedIf(
      $count > 0 ||
      !in_array(
        $field->$property,
        $this->fieldSuggestionHelper()->ignored($entity_type, $field_name),
      ),
    );
  }

  /**
   * Returns the name of the main property.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $field_name
   *   The field name.
   */
  protected function property(
    string &$entity_type,
    string $field_name,
  ): string {
    $entity_type = $this->fieldSuggestionHelper()->decode($entity_type);

    $this->definition = $this->entityFieldManager()
      ->getBaseFieldDefinitions($entity_type)[$field_name];

    return $this->definition->getMainPropertyName() ?? 'value';
  }

}
